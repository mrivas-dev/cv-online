import React, { Suspense } from "react";
import { Router, Redirect } from "@reach/router";
import "semantic-ui-css/semantic.min.css";
import { Landing } from "./pages/Landing/Landing";
import { NotFound } from "./pages/404/NotFound";
import { NavBar } from "./components/NavBar/NavBar";
import LoginModal from "./modals/LoginModal/LoginModal";
import { Context, Theme } from "./Context";
import { Profile } from "./pages/Profile/Profile";
import { CVCreator } from "./pages/CVCreator/CVCreator";
import { Footer } from "./components/Footer/Footer";
import "./App.css";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { ApolloProvider } from "@apollo/client";
import useUser from "./_hooks/useUser";
import { createUploadLink } from "apollo-upload-client";

const App = () => {
	const [theme, setTheme] = React.useState(Theme.Dark);
	const [showLogin, setShowLogin] = React.useState(false);

	const { token } = useUser();

	const uploadLink = createUploadLink({
		uri: `${process.env.REACT_APP_API_URL}`, // Apollo Server is served from port 4000
		headers: {
		  "keep-alive": "true",
		  Authorization: token
		}
	  })

	const client = new ApolloClient({
		link: uploadLink,
		cache: new InMemoryCache()
	});

	return (
		<ApolloProvider client={client}>
			<Context.Provider value={{ showLogin, setShowLogin, theme, setTheme }}>
				<Suspense fallback={<div />}>
					<LoginModal open={showLogin} />
					<NavBar />
					<Router>
						<Redirect from="/" to="landing" noThrow />
						<Landing path="landing" />
						<Profile path="profile" />
						<CVCreator path="cvCreator" />
						<NotFound default />
					</Router>
					<Footer />
				</Suspense>
			</Context.Provider>
		</ApolloProvider>
	);
};

export default App;
