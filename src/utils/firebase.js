import firebase from 'firebase'
require('firebase/auth')
var firebaseConfig = {
	apiKey: "AIzaSyByqQy-5x_ZPErnnAcndBN2Ew8SRIIRJlg",
    authDomain: "cvtucv.firebaseapp.com",
    databaseURL: "https://cvtucv.firebaseio.com",
    projectId: "cvtucv",
    storageBucket: "cvtucv.appspot.com",
    messagingSenderId: "609454736574",
    appId: "1:609454736574:web:707e19ae815c85ebc18aef",
    measurementId: "G-LJRVLH0XST"
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const google = new firebase.auth.GoogleAuthProvider();

export { auth, google };