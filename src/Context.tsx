import { createContext, useContext } from "react";

export enum Theme {
	Dark = "Dark",
	Light = "Light",
}

export type ContextType = {
	showLogin: boolean;
	setShowLogin: (show: boolean) => void;
	theme?: Theme;
	setTheme?: (Theme: Theme) => void;
};

export const Context = createContext<ContextType>({
	showLogin: false,
	setShowLogin: (login: boolean) => console.log(login),
	theme: Theme.Dark,
	setTheme: (theme) => console.warn("no theme provider"),
});
export const useFullContext = () => useContext(Context);
