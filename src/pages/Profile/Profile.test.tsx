import React from "react";
import { Profile } from "./Profile";
import { fireEvent, render } from "@testing-library/react";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
	uri: `${process.env.REACT_APP_API_URL}`,
	cache: new InMemoryCache(),
});

test("renders profile", () => {
	const { getByTestId } = render(
		<ApolloProvider client={client}>
			<Profile />
		</ApolloProvider>
	);

	const accountView = getByTestId('account-view');
	expect(accountView).toBeInTheDocument();

	const setAccountEditMode = getByTestId('account-set-edit-button');
	expect(setAccountEditMode).toBeInTheDocument();
	
	fireEvent.click(setAccountEditMode);
	const accountEdit = getByTestId('account-edit');
	expect(accountEdit).toBeInTheDocument();
	
	const personalView = getByTestId('personal-view');
	expect(personalView).toBeInTheDocument();
	
	const setPersonalEditMode = getByTestId('personal-set-edit-button');
	expect(setPersonalEditMode).toBeInTheDocument();
	
	fireEvent.click(setPersonalEditMode);
	const personalEdit = getByTestId('personal-edit');
	expect(personalEdit).toBeInTheDocument();
	
	const imageWrapper = getByTestId('image-wrapper');
	expect(imageWrapper).toBeInTheDocument();
	
	const changeImageButton = getByTestId('change-image-button');
	expect(changeImageButton).toBeInTheDocument();

});
