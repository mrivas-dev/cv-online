import React from "react";
import { Container, Grid, Image, Card, Icon, Button, Form, Input } from "semantic-ui-react";
import { gql, useQuery } from "@apollo/client";
import "./Profile.css";

const GET_ME = gql`
	query GetMe {
		me {
			id
			email
			name
			surname
		}
	}
`;

export const Profile = (props: any) => {
	const { loading, error, data } = useQuery(GET_ME);

	const [accountEditMode, setAccountEditMode] = React.useState(false);
	const [personalEditMode, setPersonalEditMode] = React.useState(false);
	const [securityEditMode, setSecurityEditMode] = React.useState(false);

	const [email, setEmail] = React.useState('');

	React.useEffect(() => {}, [data]);

	const handleEmailChange = (name:string, value: string) => value !== email ? setEmail(value) : '';

	const renderAccountView = () => (
		<div className="panelField" data-testid="account-view">
			<label>Email </label>
			<span>{data?.me?.email}</span>
		</div>
	);

	const renderAccountEdit = () => (
		<Form data-testid="account-edit">
			<Form.Field
				control={Input}
				label='Email'
				onChange={handleEmailChange}
			/>
			<Button type='submit'>Guardar cambios</Button>
		</Form>
	);

	const renderSecurityView = () => (
		<div className="panelField" data-testid="security-view">
			<label>Contraseña vieja </label>
			
		</div>
	);

	const renderSecurityEdit = () => (
		<Form data-testid="security-edit">
			<Form.Field
				control={Input}
				label='Contraseña nueva'
				onChange={handleEmailChange}
			/>
			<Button type='submit'>Guardar cambios</Button>
		</Form>
	);
	
	const renderPersonalView = () => (
		<div data-testid="personal-view">
			<Grid.Column>
				<div className="panelField">
					<label>Nombre </label>
					<span>{data?.me?.name}</span>
				</div>
			</Grid.Column>
			<Grid.Column>
				<div className="panelField">
					<label>Apellido </label>
					<span>{data?.me?.surname}</span>
				</div>
			</Grid.Column>
			<Grid.Column>
				<div className="panelField">
					<label>Fecha de nacimiento </label>
					<span>19/03/1990</span>
				</div>
			</Grid.Column>
			<Grid.Column>
				<div className="panelField">
					<label>Localidad </label>
					<span>Buenos Aires</span>
				</div>
			</Grid.Column>
		</div>
	);

	const renderPersonalEdit = () => (
		<div data-testid="personal-edit">
			<Grid.Column>
				<div className="panelField">
					<label>Nombre </label>
					<input type="text" value={data?.me?.name}/>
				</div>
			</Grid.Column>
			<Grid.Column>
				<div className="panelField">
					<label>Apellido </label>
					<input type="text" value={data?.me?.surname}/>
				</div>
			</Grid.Column>
			<Grid.Column>
				<div className="panelField">
					<label>Fecha de nacimiento </label>
					<input type="text" value="19/03/1990"/>
				</div>
			</Grid.Column>
			<Grid.Column>
				<div className="panelField">
					<label>Localidad </label>
					<input type="text" value="Buenos Aires"/>
				</div>
			</Grid.Column>
		</div>
	);

	return (
		<Container
			style={{ paddingTop: "7em" }}
			textAlign="center"
			loading={loading.toString()}
			error={error}
		>
			<Grid centered loading={loading.toString()}>
				<Grid.Row>
					<Image
						data-testid="image-wrapper"
						src="https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
						size="small"
						className="circle custom-circle"
					/>
				</Grid.Row>
				<Grid.Row>
				 <Button
						data-testid="change-image-button"
						content="Cambiar foto"
						icon="trash"
						labelPosition="right"
					/> 
				</Grid.Row>
			</Grid>

			<Grid
				style={{ paddingTop: "5em", paddingBottom: "5em" }}
				centered
				columns={2}
				loading={loading.toString()}
			>
				<Grid.Column>
					<Card>
						<Card.Content>
							<Card.Header>
								<Grid columns={2}>
									<Grid.Column>
										<Icon name="user circle" />
										Cuenta
									</Grid.Column>
									<Grid.Column>
										{ accountEditMode 
											? (
												<div 
													data-testid="account-set-view-button"
													className="actionButton pointer" 
													onClick={()=>{ setAccountEditMode(false) }}
												>
													<Icon name="close" />
												</div>
												)
											: (
												<div 
													data-testid="account-set-edit-button"
													className="actionButton pointer" 
													onClick={()=>{ setAccountEditMode(true) }}
												>Editar</div>
												)
										}
									</Grid.Column>
								</Grid>
							</Card.Header>
							<Card.Meta>Informacion de cuenta</Card.Meta>
							<Card.Description>
								<Grid columns={2}>
									<Grid.Column>
										{ accountEditMode 
											? (renderAccountEdit()) 
											: (renderAccountView())
										}
									</Grid.Column>
								</Grid>
							</Card.Description>
						</Card.Content>
					</Card>
				</Grid.Column>
				<Grid.Column>
					<Card>
						<Card.Content>
							<Card.Header>
								<Grid columns={2}>
									<Grid.Column>
										<Icon name="info circle" />
										Personal
									</Grid.Column>
									<Grid.Column>
									{ personalEditMode 
											? (
												<div 
													data-testid="personal-set-view-button"
													className="actionButton pointer" 
													onClick={()=>{ setPersonalEditMode(false) }}
												>
													<Icon name="close" />
												</div>
												)
											: (
												<div 
													data-testid="personal-set-edit-button"
													className="actionButton pointer" 
													onClick={()=>{ setPersonalEditMode(true) }}
												>Editar</div>
												)
										}
									</Grid.Column>
								</Grid>
							</Card.Header>
							<Card.Meta>Informacion personal</Card.Meta>
							<Card.Description>
								<Grid columns={2}>
									{ personalEditMode 
										? (renderPersonalEdit()) 
										: (renderPersonalView())
									}
								</Grid>
							</Card.Description>
						</Card.Content>
					</Card>
				</Grid.Column>
				<Grid.Column>
					<Card>
						<Card.Content>
							<Card.Header>
								<Grid columns={2}>
									<Grid.Column>
										<Icon name="user secret" />
										Seguridad
									</Grid.Column>
									<Grid.Column>
										<div 
											data-testid="security-set-edit-button"
											className="actionButton pointer"
											onClick={()=>{ setSecurityEditMode(true) }}
											>Editar</div>
									</Grid.Column>
								</Grid>
							</Card.Header>
							<Card.Meta>Information de seguridad</Card.Meta>
							<Card.Description>
								{ securityEditMode 
									? (renderSecurityEdit()) 
									: (renderSecurityView())
								}
								</Card.Description>
						</Card.Content>
					</Card>
				</Grid.Column>
			</Grid>
		</Container>
	);
};
