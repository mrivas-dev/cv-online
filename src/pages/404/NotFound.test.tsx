import React from "react";
import { render, screen } from "@testing-library/react";
import { NotFound } from "./NotFound";

test("renders not found text", () => {
	render(<NotFound default />);
	const linkElement = screen.getByText(
		/Oops, la página solicitada no se encuentra/i
	);
	expect(linkElement).toBeInTheDocument();
});
