import React from "react";
import { render, screen } from "@testing-library/react";
import { Landing } from "./Landing";

test("renders learn react link", () => {
	render(<Landing default />);
	const linkElement = screen.getByText(
		/Learn React/i
	);
	expect(linkElement).toBeInTheDocument();
});
