import React from "react";
import { Container, Menu } from "semantic-ui-react";
import { useFullContext } from "../../Context";
import useRedirect from "../../_hooks/useRedirect";
import useUser from "../../_hooks/useUser";
import "./NavBar.css";

export const NavBar = () => {
	const { isLoggedIn, logOut } = useUser();
	const { currentRoute, handleRedirect } = useRedirect();
	const { setShowLogin } = useFullContext();

	const renderLoginButton = () => (
		<Menu.Item
			as="a"
			onClick={() => { setShowLogin(true); }}
		>
			Ingresar
		</Menu.Item>
	);
	const renderLoggedMenu = () => (
		<>
			<Menu.Item
				as="a"
				active={currentRoute === "profile"}
				onClick={() => {
					handleRedirect("profile");
				}}
			>
				Perfil
			</Menu.Item>
			<Menu.Item
				as="a"
				active={currentRoute === "cvCreator"}
				onClick={() => {
					handleRedirect("cvCreator");
				}}
			>
				My Cv
			</Menu.Item>
			<Menu.Menu position="right">
				<Menu.Item
					as="a"
					name="Salir"
					onClick={() => {
						logOut();
						handleRedirect("");
					}}
				/>
			</Menu.Menu>
		</>
	);

	return (
		<Menu
			className="topNavbar"
			fixed="top"
			pointing
			secondary
			inverted
			size="large"
		>
			<Container>
				<Menu.Item
					as="a"
					active={currentRoute === "landing"}
					onClick={() => {
						handleRedirect("");
					}}
				>
					Inicio
				</Menu.Item>
				{!isLoggedIn ? renderLoginButton() : renderLoggedMenu()}
			</Container>
		</Menu>
	);
};
