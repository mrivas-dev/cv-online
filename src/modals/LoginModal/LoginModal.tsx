import React, { FormEvent } from "react";
import {
	Button,
	Modal,
	Form,
	Tab,
	Grid,
	Divider,
	Checkbox,
	InputOnChangeData,
	Message,
	Icon,
	Header,
} from "semantic-ui-react";
import { useFullContext } from "../../Context";
import "./LoginModal.css";
import { auth, google } from "../../utils/firebase";
import { gql, useMutation } from "@apollo/client";
import useUser from "../../_hooks/useUser";
import useRedirect from "../../_hooks/useRedirect";

const REGISTER_USER = gql`
	mutation RegisterUser(
		$name: String
		$surname: String
		$email: String!
		$password: String!
	) {
		registerUser(
			name: $name
			surname: $surname
			email: $email
			password: $password
		) {
			token
		}
	}
`;

const LOGIN = gql`
	mutation Login($email: String!, $password: String!) {
		login(email: $email, password: $password) {
			token
		}
	}
`;

const LoginModal = (open: any) => {
	const { logIn } = useUser();
	const { handleRedirect } = useRedirect();

	const { setShowLogin } = useFullContext();
	const [error, setError] = React.useState(false);
	const [username, setUsername] = React.useState("");
	const [password, setPassword] = React.useState("");

	const [name, setName] = React.useState("");
	const [surname, setSurname] = React.useState("");
	const [email, setEmail] = React.useState("");
	const [newPassword, setNewPassword] = React.useState("");
	const [termsAndConditions, setTermsAndConditions] = React.useState(false);

	const [
		registerUser,
		{ loading: registerLoading, error: registerError },
	] = useMutation(REGISTER_USER);

	const [login, { loading: loginLoading, error: loginError }] = useMutation(
		LOGIN
	);

	React.useEffect(() => {}, []);

	const panes = [
		{
			menuItem: "Ingreso",
			render: () => (
				<Tab.Pane attached={false}>
					<Grid columns={2} relaxed="very">
						<Grid.Column>
							<Header as="h2" textAlign="center">
								Ingreso utilizando redes sociales
							</Header>
							<Button disabled className="social-button" id="facebook-connect">
								<span>Connect with Facebook</span>
							</Button>{" "}
							<Button
								onClick={handleLogInWithGoogle}
								className="social-button"
								id="google-connect"
							>
								<span>Connect with Google</span>
							</Button>
							<Button disabled className="social-button" id="twitter-connect">
								<span>Connect with Twitter</span>
							</Button>{" "}
							<Button disabled className="social-button" id="linkedin-connect">
								<span>Connect with LinkedIn</span>
							</Button>
						</Grid.Column>
						<Grid.Column>
							<Header as="h2" textAlign="center">
								Ingresar utilizando email y contraseña 🔓
							</Header>
							{renderLoginContent()}
						</Grid.Column>
					</Grid>
					<Divider vertical></Divider>
				</Tab.Pane>
			),
		},
		{
			menuItem: "Registro",
			render: () => (
				<Tab.Pane attached={false}>
					<Grid columns={2} relaxed="very">
						<Grid.Column>
							<Header as="h2" textAlign="center">
								Registro utilizando redes sociales
							</Header>
							<Button disabled className="social-button" id="facebook-connect">
								<span>Connect with Facebook</span>
							</Button>{" "}
							<Button
								disabled
								onClick={handleSignUpWithGoogle}
								className="social-button"
								id="google-connect"
							>
								<span>Connect with Google</span>
							</Button>
							<Button disabled className="social-button" id="twitter-connect">
								<span>Connect with Twitter</span>
							</Button>{" "}
							<Button disabled className="social-button" id="linkedin-connect">
								<span>Connect with LinkedIn</span>
							</Button>
						</Grid.Column>
						<Grid.Column>
							<Header as="h2" textAlign="center">
								Registro rápido 🚀
							</Header>
							{renderSignUpContent()}
						</Grid.Column>
					</Grid>
					<Divider vertical></Divider>
				</Tab.Pane>
			),
		},
	];

	const handleUsername = (
		event: React.ChangeEvent<HTMLInputElement>,
		{ name, value }: InputOnChangeData
	) => setUsername(value);

	const handlePassword = (
		event: React.ChangeEvent<HTMLInputElement>,
		{ name, value }: InputOnChangeData
	) => setPassword(value);

	const handleName = (
		event: React.ChangeEvent<HTMLInputElement>,
		{ name, value }: InputOnChangeData
	) => setName(value);

	const handleSurname = (
		event: React.ChangeEvent<HTMLInputElement>,
		{ name, value }: InputOnChangeData
	) => setSurname(value);

	const handleEmail = (
		event: React.ChangeEvent<HTMLInputElement>,
		{ name, value }: InputOnChangeData
	) => setEmail(value);

	const handleNewPassword = (
		event: React.ChangeEvent<HTMLInputElement>,
		{ name, value }: InputOnChangeData
	) => setNewPassword(value);

	const toggleTermsAndConditions = () =>
		setTermsAndConditions(!termsAndConditions);

	const handleCloseLoginModal = () => {
		setShowLogin(false);
	};

	const handleLoginSubmit = async (event: FormEvent<HTMLFormElement>) => {
		if (event) {
			event.preventDefault();
		}
		setError(false);
		login({
			variables: { email: username, password },
		})
			.then((response: any) => {
				const token = response.data.login.token;
				logIn(token);
				handleCloseLoginModal();
				handleRedirect("profile");
			})
			.catch((err: any) => {
				setError(true);
			});
	};
	const handleSignUpSubmit = async (event: FormEvent<HTMLFormElement>) => {
		if (event) {
			event.preventDefault();
		}

		setError(false);
		registerUser({
			variables: {
				name,
				surname,
				email,
				password: newPassword,
			},
		})
			.then((response: any) => {
				const token = response.data.registerUser;
				logIn(token);
				handleCloseLoginModal();
				handleRedirect("profile");
			})
			.catch((err: any) => {
				setError(true);
			});
	};
	const handleLogInWithGoogle = async () => {
		auth
			.signInWithPopup(google)
			.then((response) => {
				console.log(response);
				handleCloseLoginModal();
			})
			.catch((err) => {
				switch (err.code) {
					default:
						setError(true);
				}
			});
	};
	const handleSignUpWithGoogle = async () => {};
	const renderLoginContent = () => (
		<Form onSubmit={handleLoginSubmit} loading={loginLoading} error={error}>
			<Form.Input
				fluid
				name="username"
				onChange={handleUsername}
				value={username}
				icon="user"
				iconPosition="left"
				placeholder="E-mail"
				type="email"
				data-testid="email-input"
			/>
			<Form.Input
				fluid
				name="password"
				onChange={handlePassword}
				value={password}
				icon="lock"
				iconPosition="left"
				placeholder="Contraseña"
				type="password"
				data-testid="password-input"
			/>
			<Button data-testid="login-button" animated type="submit" color="blue" fluid size="medium">
				<Button.Content visible>
					{" "}
					<Icon name="lock" /> Ingresar
				</Button.Content>
				<Button.Content hidden>
					<Icon name="unlock" />
				</Button.Content>
			</Button>

			{error && (
				<Message
					error
					header="Error"
					content={"Hubo un error al querer ingresar al sistema. " + loginError}
				/>
			)}
		</Form>
	);
	const renderSignUpContent = () => (
		<Form onSubmit={handleSignUpSubmit} loading={registerLoading} error={error}>
			<Form.Input
				fluid
				name="name"
				onChange={handleName}
				value={name}
				icon="user"
				iconPosition="left"
				placeholder="Nombre"
				type="text"
			/>
			<Form.Input
				fluid
				name="surname"
				onChange={handleSurname}
				value={surname}
				icon="user"
				iconPosition="left"
				placeholder="Apellido"
				type="text"
			/>
			<Form.Input
				fluid
				name="email"
				onChange={handleEmail}
				value={email}
				icon="mail"
				iconPosition="left"
				placeholder="Email"
				type="email"
			/>
			<Form.Input
				fluid
				name="newPassword"
				onChange={handleNewPassword}
				value={newPassword}
				icon="lock"
				iconPosition="left"
				placeholder="Contraseña"
				type="password"
			/>

			<Form.Field>
				<Checkbox
					label="I agree to the Terms and Conditions"
					checked={termsAndConditions}
					onChange={toggleTermsAndConditions}
				/>
			</Form.Field>

			<Button data-testid="signin-button" animated type="submit" color="green" fluid size="medium">
				<Button.Content visible>
					{" "}
					<Icon name="user plus" /> Registrarme
				</Button.Content>
				<Button.Content hidden>
					<Icon name="save" />
				</Button.Content>
			</Button>

			{error && (
				<Message
					error
					header="Error"
					content={
						"Hubo un error al querer ingresar al sistema. " + registerError
					}
				/>
			)}
		</Form>
	);

	return (
		<Modal size="large" open={open.open}>
			<Button
				data-testid="close-button"
				className="closeButton"
				circular
				icon="close"
				onClick={() => {
					handleCloseLoginModal();
				}}
			/>
			<Modal.Content scrolling>
				<Tab
					className="tabWrapper"
					menu={{ secondary: true, pointing: true }}
					panes={panes}
				/>
			</Modal.Content>
		</Modal>
	);
};

export default LoginModal;
