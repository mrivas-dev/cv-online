import React from "react";
import { fireEvent, render } from "@testing-library/react";
import LoginModal from "./LoginModal";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
	uri: `${process.env.REACT_APP_API_URL}`,
	cache: new InMemoryCache(),
});

const mockUser = {
	email: "testuser@test.com",
	password: "test",
};

test("modal renders tab panes titles", () => {
	const { getByText } = render(
		<ApolloProvider client={client}>
			<LoginModal open={true} />
		</ApolloProvider>
	);

	const renderLoginPaneTitle = getByText("Ingreso");
	const renderSignUpPaneTitle = getByText("Registro");
	expect(renderLoginPaneTitle).toBeInTheDocument();
	expect(renderSignUpPaneTitle).toBeInTheDocument();
});

test("modal renders login titles", () => {
	const { getByText } = render(
		<ApolloProvider client={client}>
			<LoginModal open={true} />
		</ApolloProvider>
	);

	const renderLoginH2Social = getByText("Ingreso utilizando redes sociales");
	const renderLoginH2Email = getByText(
		"Ingresar utilizando email y contraseña 🔓"
	);

	expect(renderLoginH2Social).toBeInTheDocument();
	expect(renderLoginH2Email).toBeInTheDocument();
});

test("modal renders sign up titles", async () => {
	const { getByText } = render(
		<ApolloProvider client={client}>
			<LoginModal open={true} />
		</ApolloProvider>
	);

	const signUpButtonPane = getByText("Registro");

	await fireEvent.click(signUpButtonPane);

	const renderSignUpH2Social = getByText("Registro utilizando redes sociales");
	const renderSignUpH2Email = getByText("Registro rápido 🚀");
	expect(renderSignUpH2Social).toBeInTheDocument();
	expect(renderSignUpH2Email).toBeInTheDocument();
});

test("login fails if credentials are wrong", async () => {
	const { email, password } = mockUser;
	const { getByTestId } = render(
		<ApolloProvider client={client}>
			<LoginModal open={true} />
		</ApolloProvider>
	);

	const emailInput = getByTestId("email-input").children[0];
	const passwordInput = getByTestId("password-input").children[0];

	expect(emailInput).toBeInTheDocument();
	expect(passwordInput).toBeInTheDocument();

	fireEvent.change(emailInput, { target: { value: email } });
	expect(emailInput).toHaveValue(email);

	fireEvent.change(passwordInput, { target: { value: password } });
	expect(passwordInput).toHaveValue(password);

	// const loginButtonPane = getByTestId("login-button");
	// await fireEvent.click(loginButtonPane);
	// const errorMessage = getByText("Error");
	// expect(errorMessage).toBeInTheDocument();
});
