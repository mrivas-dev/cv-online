import * as React from "react";
import Cookies from "js-cookie";

export default function useUser() {
	const [token, setToken] = React.useState(() => {
		const tk = Cookies.get("token");
		if (tk) {
			return tk;
		} else {
			return "";
		}
	});

	const [isLoggedIn, setIsLoggedIn] = React.useState(() => {
		if (Cookies.get("token")) {
			return true;
		} else {
			return false;
		}
	});

	const logIn = (token: string) => {
		Cookies.set("token", token);
		setIsLoggedIn(true);
		setToken(token);
	};

	const logOut = () => {
		Cookies.remove("token");
		setIsLoggedIn(false);
	};

	return { token, isLoggedIn, logIn, logOut };
}
