import * as React from "react";

export default function useRedirect() {
	const [currentRoute, setCurrentRoute] = React.useState("");
	const params = window.location.href.split("/");

	React.useEffect(() => {
		const route =
			params && params.length ? params[params.length - 1].split("?")[0] : "";
		setCurrentRoute(route);
	}, [params]);

	const handleRedirect = (path = "") => {
		if (currentRoute !== path) {
			window.location.href = `./${path}`;
		}
	};

	return { currentRoute, handleRedirect };
};
